from django.urls import path, include
from .views import (
    TodoListApiView,
    ToDoDetailApiView,
    WeatherApiView
)

urlpatterns = [
    path('todo', TodoListApiView.as_view()),
    path('todo/<int:todo_id>', ToDoDetailApiView.as_view()),
    path('weather/<city>', WeatherApiView.as_view()),
]