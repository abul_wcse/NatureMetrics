from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from .models import Todo
from .serializers import TodoSerializer
from geopy.geocoders import Nominatim

import requests

class TodoListApiView(APIView):
    # add permission to check if user is authenticated
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        todos = Todo.objects.filter(user = request.user.id)
        serializer = TodoSerializer(todos, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        data = {
            'task': request.data.get('task'),
            'completed': request.data.get('completed'),
            'user': request.user.id
        }
        serializer = TodoSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ToDoDetailApiView(APIView):
    permissions_class = [permissions.IsAuthenticated]

    def get(self, request, todo_id, *args, **kwargs):
        return Response('{"status":"ok"}', status=status.HTTP_200_OK)

class WeatherApiView(APIView):
    permissions_class = [permissions.IsAuthenticated]

    def get(self, request,city,*args, **kwargs):
        geolocator = Nominatim(user_agent="nature-metrics")
        country = "UK"
        loc = geolocator.geocode(city + ',' + country)
        apiurl = 'https://api.open-meteo.com/v1/forecast?latitude='+str(round(loc.latitude,2))+'&longitude='+str(round(loc.longitude,2))+'&hourly=temperature_2m'
        print(apiurl)
        response = requests.get(apiurl)
        return Response(response.json(), status=status.HTTP_200_OK)